%!TeX spellcheck = de-DE, en-US
%Schriftgröße, Layout, Papierformat, Art des Dokumentes
\documentclass[12pt,a4paper,titlepage]{article}
%Einstellungen der Seitenränder
\usepackage[inner=3.0cm,outer=3cm,top=2.5cm,bottom=3cm]{geometry}
\usepackage[ngerman]{babel}
\usepackage[utf8]{inputenc}
% hyperref mit sinnvollen einstellungen
\usepackage[pdftex,
        colorlinks=true,
        urlcolor=black,                       % \href{...}{...} external (URL)
        filecolor=black,                      % \href{...} local file
        linkcolor=black,                      % \ref{...} and \pageref{...}
        citecolor=black,
        pdfauthor={Jakob Huber},
        pdfkeywords={},
        pdfproducer={pdfLaTeX},
    	%pdfadjustspacing=1,
        plainpages=false,
        pdfpagelabels,
        pagebackref,
        pdfpagemode=UseOutlines,
        pdfstartview={FitV},
        bookmarksopenlevel=section,
        bookmarksopen=false]{hyperref}
% meist verwendetes Grafik-Paket
\usepackage[pdftex]{graphicx}
% weitere Pakete
\usepackage{subcaption}
\usepackage{tipa}
\usepackage[
intlimits,
sumlimits
]{amsmath}

\def\UrlBreaks{\do\/\do-}

%Kopf- und Fußzeile
\usepackage{fancyhdr}
\usepackage{amssymb}
\fancyhf{}

% Informationen zur Arbeit
\title{{SYSAD Projektarbeit}}
\author{Janis Wellenberger, Lea Brüggemann, Jakob Huber}
%\date{ 29th, 2018}

%\setcitestyle{square}

\begin{document}

% no indent
\setlength{\parindent}{0pt}

% Einbinden der Titelseite
\input{title.tex}
\cleardoublepage

% Seitenstil
\pagestyle{fancy}

\newpage

\fancyhead[
RO]{\nouppercase{\leftmark}}
\fancyfoot[RO]{\thepage}

%Linie oben
\renewcommand{\headrulewidth}{0.5pt}

% Zähler für Seiten
\setcounter{page}{1}

\begin{abstract}
    Diese Arbeit zeigt, dass es möglich ist IoT mit eigener Open-Source-Infrastruktur zu betreiben, ohne auf Komfortfunktionen verzichten zu müssen.\\

    Als IoT-Gerät wurde eine Wetterstation entwickelt bestehend aus einer Sensoreinheit und einem Gateway.
    Die Sensoreinheit verfügt über einen ESP8266-Mikrocontroller, einen BME280-Umgebungssensor und einen Regensensor.
    Der Softwarestack des Gateways besteht aus Grafana, InfluxDB, Mosquitto und Telegraf, ist in Docker umgesetzt und wird auf einem Raspberry~PI~4B betrieben.\\

    Die Messgenauigkeit der Sensoreinheit liegt bei
    \begin{itemize}
        \setlength\itemsep{0em}
        \item $\pm 3\%$ für Luftfeuchtigkeit
        \item $\pm 1hPa$ für Luftdruck
        \item $\pm 1.25^\circ C$ für Temperatur
        \item Ja / Nein für Regen
    \end{itemize}
\end{abstract}

\newpage

% BEGINN INHALT **************************************************************
\fancyhead[RO]{Wetterstation}

\tableofcontents

\listoffigures

\newpage

\section{Projektidee}
Innerhalb dieser Projektarbeit wird die Entwicklung einer Wetterstation beschrieben, die aktuelle Wetterdaten erfasst, speichert und visuell aufbereitet.
Die aufbereiteten Daten sollen für den Nutzer schnell und einfach einsehbar sein.\\
Die Wetterstation erfasst Temperatur, Luftdruck, Luftfeuchtigkeit sowie Niederschlag.
Das ganze System wird ohne Cloud-Anbindung umgesetzt.

\section{Motivation}
Mit diesem Projekt soll gezeigt werden, dass IoT ohne Cloud und ohne Abomodell umgesetzt werden kann.
Denn auch wenn lokale Wetterdaten nicht personenbezogen sind, müssen sie nicht in der Cloud landen.\\
Uns war es wichtig, dass am Ende des Projekts ein Produkt entsteht, welches einen tatsächlichen Nutzen für uns hat.
Als Hobbygärtner pflanzen wir selbst Gemüse, Kräuter und Beeren auf dem Balkon an.
Entsprechend wünschen wir uns mit der Hilfe der Wetterstation einen größeren Ertrag da die Pflanzen zum optimalen Zeitpunkt gepflanzt werden können.\\
Diese Projektarbeit wurde innerhalb des Moduls Systemadministration verfasst.

\newpage

\section{Grundbegriffe}

\subsection{Container}
Ein Container ist ein Softwarepaket mit allem was die Software zum Funktionieren benötigt.
Dieses Paket besitzt neben dem ausführbaren Programm, auch benötigte Bibliotheken und Einstellungen.
Im Gegensatz zu herkömmlichen Programmen, läuft ein Container isoliert von anderer Software, so auch vom Betriebssystem.\cite{containerDef}\\
Im Vergleich zur normalen Virtualisierung gibt es bei der Containervirtualisierung keine Abstraktionsschicht zwischen dem virtualisierten System und dem Host-Kernel.
Dadurch ist die Containervirtualisierung sehr ressourcensparend.\cite{dockerdef}

\subsection{IoT}
\textit{Internet of Things} (dt. Internet der Dinge) bezeichnet Systeme, deren Geräte über das Internet verbunden sind.\cite{iotdef}

\subsection{MQTT}
\textit{Message Queuing Telemetry Transport} ist ein Nachrichtenprotokoll für IoT.
Die Architektur besteht aus MQTT-Clients und einem Broker.
MQTT-Clients veröffentlichen Daten zu einem festgelegten Thema (\textit{topic}) auf den MQTT-Broker oder abonnieren beliebige viele dieser Themen.
Der Broker vermittelt die auf ihm veröffentlichten Daten zu den entsprechenden Abonnenten.\cite{mqttmosdef}

\subsubsection*{I²C Protokoll}
Das I²C (\textit{Inter-Integrated Circuit}) Protokoll von NPX wird zur bidirektionalen Kommunikation zwischen verschiedenen ICs verwendet.
Neben einer hohen Übertragungsrate von bis zu 5Mbit/s hat I²C den Vorteil nur zwei Kabel zur Datenübertragung zwischen vielen Geräten benötigt werden.\cite{i2cSpec}


\subsection{Softwarestack}

\subsubsection{Docker}
Docker ist eine Open-Source-Software zur Containervirtualisierung.
Sie ermöglicht \textit{Docker Images} (Speicherabbilder des zu virtualisierenden Systems) zu speichern und über \textit{Registries} zu verbreiten.
Für viele Dienste werden bereits fertige \textit{Docker Images} zur Verfügung gestellt.\\
Die laufende Instanz eines \textit{Docker Images} wird als \textit{Docker Container} bezeichnet.\cite{dockerdef}

\subsubsection{Docker Compose}
Docker Compose ist ein Werkzeug, um das Definieren und Ausführen mehrerer \textit{Docker Container} zu vereinfachen.\cite{dockercomposedef}
In einer \textit{docker-compose.yml}-Datei werden alle notwendigen Informationen spezifiziert.\\
Dort wird unter Anderem festgelegt:
\begin{itemize}
    \item welche Images instanziiert werden
    \item in welcher Reihenfolge die Images instanziiert werden
    \item welche Ports offengelegt werden, das heißt unter welchen Ports der Container ansprechbar ist
    \item welche Dateien in den Container gemountet werden
    \item welche Umgebungsvariablen im Container gesetzt sind
\end{itemize}

\subsubsection{Grafana}
Eine Open-Source-Webanwendung zur Visualisierung von Daten, die es erlaubt eine Vielzahl von Datenbanken einzubinden.
Grafana kann Datenbankanfragen abschicken und die gelieferten Daten direkt auf Dashboards in verschiedenen Graphen darstellen.
Art und Aussehen der Graphen kann dabei an die Bedürfnisse angepasst werden.\cite{grafanaedef}

\subsubsection{InfluxDB}
InfluxDB ist eine Open-Source-Datenbank die für Sensormesswerte ausgelegt ist.
Daten lassen sich sehr unkompliziert mithilfe des \textit{Influx Line Protokolls} in die Datenbank schreiben.

\subsubsection{Mosquitto}
Ein Open-Source-MQTT-Broker von der \textit{Eclipse Foundation}\cite{mqttmosdef}

\subsubsection{Telegraf}
Ein Open-Source-Server-Agent zum Sammeln, Verarbeiten und Weiterleiten von Daten.
Dafür verwendet Telegraf Input-, Processor-, Aggregator- und Output-Plugins.
Insgesamt stehen mehr als 200 Plugins zur Verfügung.\cite{telegrafedef}

\subsection{Hardware}

\subsubsection{WEMOS D1 mini}
Das \textit{WEMOS D1 mini development board} ist eine kleine Platine mit aufgelötetem ESP8266, Spannungswandler und USB-Anschluss.\cite{wemosD1MiniHomePage}

\subsubsection{ESP8266}
Der ESP8266 ist ein WLAN-fähiger 32-bit-Mikrocontroller von Espressif.
Er verfügt über einen Tiefschlafmodus in dem der Stromverbrauch auf bis zu $20\mu A$ reduziert werden kann.\cite{esp8266Spec}

\subsubsection{BME280}
Der BME280 ist ein Sensor für Luftfeuchtigkeit, Luftdruck und Temperatur von Bosch.
Er kann mittels I²C angesprochen werden.\cite{bme280Spec}

\subsubsection{MH-RD}
Der MH-RD ist ein einfacher Niederschlagssensor.
Er reagiert auf Änderungen des Widerstands einer Messplatte.
Landen Regentropfen auf der Messplatte sinkt der Widerstand und der Sensor setzt seinen Ausgang auf \textit{LOW}\cite{mhRdSpec}

\newpage

\section{Problem}
Viele handelsübliche Wetterstationen speichern ihre Daten nicht ab oder verwenden eine eigene Cloud-Umgebung.\cite{tempestWeatherSystem,netatmoWeatherStation}
In vielen Situationen sind Daten aus der Vergangenheit aber relevant.
So stellt sich zum Beispiel in der Hobbygärtnerei oft die Frage, ab wann Pflanzen in der Nacht Kälteschutz benötigen.
Dafür sind die Informationen aus dem Wetterbericht nicht genau genug an den Standort angepasst.
Lokale Faktoren wie ein (un)geschützter Bereich werden dabei nicht berücksichtigt.

\section{Anforderungen}
Die Wetterstation muss in regelmäßigen Intervallen die aktuellen Wetterdaten messen und speichern.
Dieses Intervall soll in der Praxis erprobt werden und wird zwischen zehn und 60 Minuten betragen.\\
Zu erfassen sind Temperatur, Luftdruck, Luftfeuchtigkeit sowie Niederschlag.
Zuletzt genanntes beschränkt sich auf einen binären Wert, der ausschließlich aussagt, ob der Sensor nass ist.
Die maximale Abweichung der Luftfeuchtigkeit soll unter 5~\% liegen, die der Temperatur unter 1.5~°C und die des Luftdrucks unter 3~hPa.\\
Damit ein Vergleich zur vorherigen Saison möglich ist, muss eine Datenspeicherung von mindestens zwei Jahren möglich sein.
Die aufbereiteten Daten müssen über einen modernen Webbrowser abrufbar sein und in Diagrammen angezeigt werden.
Dabei soll der dargestellte Zeitraum frei wählbar sein.
Für die Einheiten der Messwerte wird das metrische System verwendet.\\
Alle Sensoren sollen auch ohne Anschluss ans Stromnetz betriebsfähig sein und müssen der Witterung standhalten können.
Laufzeit der Stromversorgung soll mindestens 3 Monate betragen.
Zum Ende der Laufzeit soll eine Warnung ausgegeben werden.\\
Es darf nicht notwendig sein, dass Daten das Heimnetz verlassen.
Die Hard- und Software soll nicht proprietär sein.

\newpage

\section{Lösungsvorschläge}

\subsection{Architektur}
Eine mögliche Lösung ist zwei Geräte zu betreiben, ein Gateway und eine Sensoreinheit (siehe Abbildung~1).
Das Gateway nimmt dabei die Daten entgegen, schreibt sie in eine Datenbank und stellt eine Webanwendung zur Darstellung der Daten bereit.
Die verschiedenen Anwendungen hierfür können direkt auf einem Raspberry~Pi installiert oder mit einem Docker-Setup betrieben werden.
Die Sensoreinheit hat in diesem Fall nur die Aufgabe die Messwerte der Sensoren auszulesen und an das Gateway zu übermitteln.

\begin{figure}[htbp]
    \centering
    \includegraphics[width=\textwidth]{img/diagram_a}
    \caption{Aufbau von Lösungsvorschlag 1}
\end{figure}

Eine Alternative ist nur ein Gerät zu entwickeln, das sowohl Messwerte der Sensoren ausliest als auch die Datenverwaltung übernimmt (siehe Abbildung~2).
Auch in diesem Fall können die Anwendungen direkt auf einem Raspberry~Pi oder in einem Docker-Setup betrieben werden.

\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.65\textwidth]{img/diagram_b}
    \caption{Aufbau von Lösungsvorschlag 2}
\end{figure}

\subsection{Sensoren}
Für die Sensoren stehen mehrere Modelle zur Verfügung.\\
Für die Messung von Temperatur, Luftdruck und Luftfeuchtigkeit bieten sich ein BMP180 oder BME280 an.
Der BME280 ist der Nachfolger vom BMP180 und liefert eine höhere Messgenauigkeit.\cite{bmp180Spec,bme280Spec}\\
Um den Niederschlag zu messen, kann ein MH-RD Sensor oder ein komplexerer Sensor, der mit dem Wippverfahren arbeitet, verwendet werden.\cite{misolRainGauge}

\subsection{Stromversorgung}
Um die Sensoreinheit ohne Netzteil zu betreiben, muss die Stromversorgung entweder mit Batterien oder Akkus umgesetzt werden.

Batterien sind günstiger in der Anschaffung und stecken Temperaturschwankungen gut weg.
Auf Dauer und mit hohem Bedarf sind sie aber kostspielig, da nach jeder Entladung eine neue verwendet werden muss.
Ihre Ladung halten sie bei einer passiven Nutzung verhältnismäßig lange.

Akkus lassen sich nach ihrer Entladung erneut aufladen.
Deshalb sind sie für Geräte sinnvoll, die viel im Betrieb sind, oder einen relativ hohen Standby-Verbrauch haben.
Denn während die Anschaffungskosten im Vergleich zu einer Batterie höher sind, spart man hier auf lange Sicht.\cite{batVsAk}

\subsection{Kommunikation}
Für die Datenübertragung von Sensoreinheit zu Gateway stehen verschiedene Protokolle zur Verfügung.\\
So kann die Sensoreinheit mittels REST die Daten an vom Gateway bereitgestellte Endpunkte senden.
Alternativ ist auch eine Datenübertragung über MQTT möglich.\\
Für beide Protokolle existieren Bibliotheken, welche für die Sensoreinheit verwendet werden können.
Auf der Serverseite gibt es für MQTT den Mosquitto-Broker.
Ein REST-Server lässt sich problemlos in verschiedensten Sprachen umsetzen.\\
Falls nur ein Gerät entwickelt wird, ist eine Datenübertragung nicht notwendig.

\subsection{Datenverwaltung}
Da die Wetterstation Daten über die Zeit erfasst, kann eine Timeseries-Datenbank verwendet werden.
Hier stehen mit einer PostgreSQL/TimescaleDB oder InfluxDB verschiedene Systeme zur Auswahl.
Als Alternative zu einer Timeseries-Datenbank ist ein ELK-Stack eine Option.\cite{elkWeatherStation}\\
Bei der Verwendung von MQTT können die Daten mit Telegraf in die Datenbank geschrieben werden.

\subsection{Visualisierung}
Für die Visualisierung können Grafana oder Kibana verwendet werden.\\
Grafana unterstützt dabei verschiedene Datenquellen.
Die Verwendung von Kibana ergibt vor allem in einem ELK-Stack Sinn.

\subsection{Hardware}
In einer Lösung mit zwei Geräten kann ein Raspberry~Pi oder beliebiger Linux-System als Gateway dienen.\\
Für den Mikrocontroller der Sensoreinheit ist ein ESP8266 oder ESP32 möglich.\\
Wird nur ein Gerät zur Erfassung und Verarbeitung der Daten gewählt, dann kann dafür ein Raspberry~Pi verwendet werden.

\newpage

\section{Auswahl Lösung anhand Anforderungen}

\begin{figure}[htbp]
    \centering
    \includegraphics[width=\textwidth]{img/diagram}
    \caption{Aufbau der gewählten Lösung}
\end{figure}

\subsection{Architektur}
Als Systemarchitektur haben wir uns für zwei getrennte Geräte entschieden (siehe Abbildung~3).
So lässt sich das System zudem einfach mit weiteren Sensoreinheiten erweitern, was zur Flexibilität beiträgt.
Bei nur einem Gerät müsste der Raspberry~Pi mit den Sensoren draußen betrieben werden.
Da ein Raspberry~Pi aber nicht vernünftig ohne Netzteil betrieben werden kann, ist das für uns keine akzeptable Lösung.\cite{rpiPowerConsumption}

\subsection{Sensoren}
Wir haben uns für den Sensor BME280 entschieden, da dieser eine etwas höhere Messgenauigkeit besitzt.\\
Für die Niederschlagsmessung haben wir einen MH-RD Niederschlagssensor gewählt, da ein komplexerer Sensor über den Rahmen des Projektes hinausgehen würde.
Für unsere Zwecke ist eine binäre Erfassung des Niederschlags ausreichend.

\subsection{Stromversorgung}
Als Stromversorgung für die Sensoreinheit werden wir Batterien verwenden.
Da die Sensoren größeren Temperaturschwankungen ausgesetzt sein werden, sind Li-Ion oder Li-Po Akkus ungeeignet.
Ein weiterer Grund ist die lange Laufzeit, weil Batterien weniger zur Selbstentladung neigen.

\subsection{Kommunikation}
Zur Kommunikation zwischen Gateway und Sensoreinheit verwenden wir MQTT\@.
MQTT ist der Standard zur Kommunikation zwischen IoT-Geräten und ist für die Verwendung mit Mikrocontrollern optimiert.
Als MQTT-Broker verwenden wir Mosquitto.

\subsection{Datenbank}
Um die Daten aus Mosquitto in die Datenbank zu übertragen werden wir Telegraf einsetzen.\\
Als Datenbank verwenden wir InfluxDB.
Hier ist die Integration mit Telegraf einfacher und die Kompatibilität mit einem Raspberry~Pi sicher gestellt.

\subsection{Visualisierung}
Zur Darstellung der Messdaten nutzen wir Grafana.
Grafana ermöglicht direkten Zugriff auf die Daten in der InfluxDB\@.\\
Zudem ist Kibana nicht für die Nutzung mit einer InfluxDB gedacht.

\subsection{Hardware}
Auf dem Gateway ist ein Docker-Setup eine sehr flexible Lösung.
Damit könnte ein beliebiges Linux-System als Gateway fungieren.
Wir werden einen Raspberry~Pi verwenden.\\
Für die Sensoreinheit benutzen wir den günstigeren ESP8266, da die zusätzliche Rechenleistung eines ESP32 nicht notwendig ist.\cite{esp32Spec}

\newpage

\section{Umsetzung}

\subsection{Bau der Sensoreinheit}
Im ersten Schritt der Umsetzung wurde die Sensoreinheit gebaut.
Da wir keine eigene Platine entwerfen, verwenden wir ein WEMOS D1 mini als Plattform.
Wir haben dieses Development-Board gewählt, weil dafür eine Lochplatine verfügbar ist auf die wir unsere Komponenten auflöten können.\\

\begin{minipage}{0.55\textwidth}
    \includegraphics[width=\textwidth]{img/hardware}
\end{minipage}%
\hfill%
\begin{minipage}{0.4\textwidth}
    \begin{itemize}
        \item[\textbf{a.}] WEMOS D1 mini
        \item[\textbf{b.}] Prototype Shield
        \item[\textbf{c.}] MH-RD Regensensor
        \begin{itemize}
            \item[\textbf{1.}] Sensorplatte
            \item[\textbf{2.}] Signalwandler
        \end{itemize}
        \item[\textbf{d.}] BME280 Sensor
    \end{itemize}
\end{minipage}


\begin{figure}[htbp]
    \centering
    \includegraphics[width=\textwidth]{img/sensorunit_schematic}
    \caption{Schaltplan der Sensoreinheit}
\end{figure}

Die Transistorschaltung aus \textit{Q1, R1, R2} wird benötigt, um den Regensensor von der Versorgungsspannung zu trennen, wenn keine Messung durchgeführt wird.
Ohne diese Schaltung würde der Regensensor die Batterie stärker belasten.
Ist \textit{D7} auf \textit{HIGH} geschaltet wird der Regensensor aktiviert und an \textit{D8} kann ein digitales Signal eingelesen werden.\\

Der BME280-Sensor wird direkt an den Pins \textit{D1} und \textit{D2} angeschlossen und über das I²C-Protokoll angesprochen.
Dadurch können mit nur zwei Datenleitungen alle Funktionen des Sensors verwendet werden.\\

Um die Batteriespannung zu messen wird sie mit dem Spannungsteiler aus \textit{R5, R6} auf 0-3,3V reduziert.
Diese Reduktion ist notwendig, weil der Pin \textit{A0} maximal 3.3V aufnehmen kann.
Eine Spannung von 4.5V würde einen Defekt im Chip verursachen und diesen unbrauchbar machen.\cite{esp8266Spec}\\
Der Spannungsteiler hat Probleme bereitet, da der ESP8266 an Pin \textit{A0} einen internen Spannungsteiler besitzt der die Spannung von 0-3,3V auf 0-1V reduziert.
Das hat zur Folge, dass die Formel eines Spannungsteilers\cite{kuphaldt2006lessons} $U_2 = \frac{R_2}{R_1 + R_2} \times U_{ges}$ falsche Ergebnisse liefert.\\

Gegeben: $U_{ges}=4,5V$ $U_2=3,3V$ $R_2=1M\Omega$ $R3=220k\Omega$ $R4=100k\Omega$\\
Gesucht: $R_1$\\
Aufgrund der Reihen- und Parallelschaltung aus \textit{$R_2, R_3, R_4$} ergibt sich:
\begin{equation}
    R_{2_{eff}} = \left(\frac{1}{R_3 + R_4} + \frac{1}{R_2}\right)^{-1} = \left(\frac{1}{320k\ensuremath{\Omega} }+ \frac{1}{1000k\ensuremath{\Omega} }\right)^{-1} \approx 240k\ensuremath{\Omega}
\end{equation}
\begin{minipage}{0.5\textwidth}
    \includegraphics[width=\textwidth]{img/voltage_divider.pdf}
\end{minipage}%
\hfill%
\begin{minipage}{0.45\textwidth}
    \begin{equation}
        R_1 = \frac{R_{2_{eff}} \times U_{ges}}{U_2} - R_{2_{eff}}
    \end{equation}
    \begin{equation}
        R_1 = \frac{240k\ensuremath{\Omega} \times 4,5V}{3,3V} - 240k\ensuremath{\Omega}
    \end{equation}
    \begin{equation}
        R_1 \approx 87,3k\ensuremath{\Omega}\\
    \end{equation}

    Gewählt wurde $R_1 = 100k\Omega$ da dies der nächste übliche Widerstandswert größer $87k\Omega$ ist.
\end{minipage}

Mit $R_1 = 100k\Omega$ und $R_{2_{eff}} = 240k\Omega$ ergibt sich $U_2 = \frac{240k\ensuremath{\Omega}}{100k\ensuremath{\Omega}+240k\ensuremath{\Omega}} \times 4,5V = 3,18V$\\

Aufgrund von Platzmangel auf der Lochplatine ist es nicht möglich den Spannungsteiler von der Versorgungsspannung zu trennen.
Dadurch wird die Batterie mit dauerhaft $I_{divider} = \frac{4,5V}{100k\ensuremath{\Omega}+240k\ensuremath{\Omega}} \approx 0.01mA$ belastet.

\newpage

\subsection{Aufsetzen der Docker-Umgebung auf dem Raspberry~Pi}
Auf dem Raspberry~Pi verwenden wir das Betriebssystem \textit{Raspberry Pi OS Lite}.
Wir betreiben den Pi \textit{headless}.
Das heißt wir administrieren über SSH und ohne direkt angeschlossene Ein- oder Ausgabegeräte.\\
Der Pi meldet sich im Heimnetzwerk an, hört auf MQTT-Nachrichten von der Sensoreinheit und stellt das Webinterface bereit, über das die Daten eingesehen werden können.\\
Um die Docker-Umgebung aufzusetzen, verwenden wir \textit{Docker Compose}.\\
Insgesamt werden vier verschiedene Images verwendet;
\begin{itemize}
    \item\textit{eclipse-mosquitto:1.6}
    \item\textit{telegraf:1.16}
    \item\textit{influxdb:1.8}
    \item\textit{grafana/grafana:7.3.3}
\end{itemize}
Wir verwenden jeweils die zum jetzigen Zeitpunkt aktuellste stabile Version der Images, nicht die beim Hochfahren aktuellste.
So verhindern wir, dass es durch Updates zu Kompatibilitätsproblemen kommt.\\
Die \textit{.conf}-Dateien, die für Mosquitto und Telegraf werden in die entsprechenden Container gemountet.
Dadurch lassen sie sich leicht anpassen ohne das der Container gestartet werden muss.
Ebenso wird der persistente Speicher für die InfluxDB und die Grafana-Dashboards in die Container gemountet.\\
Nutzernamen und Passwörter sowie der Name der Datenbank werden über Umgebungsvariablen gesetzt.
Um die Container unkompliziert hochfahren zu können haben wir ein Shell-Script geschrieben, das die notwendigen Umgebungsvariablen setzt und dann Docker Compose startet.

\newpage

\subsection{Programmieren Sensoreinheit}
Um die Sensoreinheit zu programmieren, wird die IDE-Erweiterung \textit{PlatformIO} verwendet.
Diese ermöglicht das Einbinden einer Arduino-, MQTT-Client- sowie BME280-Bibliothek.\\

Wir haben uns für einen modularen Ansatz entschieden.
Entsprechend wird für den Status der Batterie, jeden Sensor und den MQTT-Client ein eigenes Modul verwendet.

\begin{figure}[htbp]
    \centering
    \includegraphics[width=\textwidth]{img/dependencies}
    \caption{Abhängigkeitsdiagramm Sensoreinheit}
\end{figure}

Die \textit{main.cpp} dient dabei als Controller.
Nach dem Tiefschlaf verbindet sich die Einheit mit dem WLAN und lädt die Messwerte aus den anderen Modulen.
Anschließend werden diese Daten über WLAN und mithilfe des MQTT-Client-Moduls an den MQTT-Broker gesendet.\\

In \textit{secrets.h} sind Makros definiert um die SSID und das Passwort zu setzen.
Diese Daten wurden ausgelagert, damit die Datei mit einem Eintrag in der \textit{.gitignore} nicht in das Git-Repository übertragen wird.

Die \textit{WIFIConfig.h} ermöglicht es weitere Einstellungen vorzunehmen.
So kann hier DHCP aktiviert oder deaktiviert werden und Lokale-, Gateway- und DNS-Adresse(n) sowie Subnetzmaske gesetzt werden.

Außerdem muss die Adresse des MQTT-Brokers in \textit{main.cpp} angepasst werden.\\

Das BME280-Modul erbt von \textit{Adafruit\_BME280}.
Das ist eine externe Bibliothek, die Funktionen zum Auslesen der Temperatur, Luftfeuchtigkeit und Luftdruck bereitstellt.
Mit \textit{readTemperature()} wird so auf die aktuelle Messung der Temperatur zugegriffen.
Das Auslesen der anderen Werte erfolgt äquivalent.
Zurückgegeben werden die Messwerte in einer \textit{map} mit dem Datentyp \textit{String} als Key und \textit{float} als Value.

Für den Niederschlagsensor und die Batteriespannung gibt es keine vorgefertigte Bibliothek.
Deswegen müssen die verwendeten Pins angegeben werden.\\
Beim Niederschlagsensor wird jeweils ein Pin zum Einschalten des Sensors und zum Auslesen des Signals benötigt.
Für die Batteriespannung wird nur eine Pin am ADC benötigt.
Der Modus der Pins für das Lesen muss auf \textit{INPUT} gestellt werden.
Der des anderen auf \textit{OUTPUT}.
Mit diesen Angaben kann in Kombination mit einem \textit{digitalRead()} Niederschlag und mit einem \textit{analogRead()} der Batteriestatus erfasst werden.
Das Ein-/Ausschalten wird mit einem \textit{digitalWrite()} und \textit{delay()} realisiert.\\

Gesendet wird über Funktionen der externen Bibliothek \textit{PubSubClient}.
Hierfür stellen wir erst eine Verbindung mit dem Mosquitto-Broker sicher.
Nach einer erfolgreichen Verbindung schreiben wir die Messwerte in eine Nachricht mit der Syntax des \textit{Influx Line Protokolls}.

\begin{verbatim}
+-----------+--------+-+---------+-+---------+
|measurement|,tag_set| |field_set| |timestamp|
+-----------+--------+-+---------+-+---------+
\end{verbatim}

\textit{measurement} gibt den Namen der Messung an, der die Werte angehören.

Durch \textit{tag\_set} lassen sich die Messdaten der gleichen Messung noch einmal unterteilen.
Das kann hilfreich sein, falls zum Beispiel mehrere Sensoreinheiten verwendet werden.

Der eigentliche Inhalt steht in \textit{field\_set}.
Erst gibt man den Spaltennamen und dann seinen Wert nach einem Ist-gleich-Zeichen an.
Hier können beliebig viele dieser Paare durch ein Komma getrennt hintereinander aufgelistet werden.

Der \textit{timestamp} ist optional und wird nicht von uns verwendet, da die InfluxDB bereits einen Zeitstempel für jeden eingetragenen Datensatz speichert.\cite{influxDBLineFormat}

\subsection{Konfiguration der einzelnen Docker-Container}

\subsubsection{Mosquitto}
Zusätzlich zur default-Konfiguration wird eine benutzerdefinierte \textit{.conf}-Datei eingebunden.
Wir fügen Mosquitto eine persistente Speicherung hinzu.
%Diese beinhaltet für dieses Projekt folgende Ergänzungen:
%\begin{verbatim}
%persistence true
%persistence_location /mosquitto/data/
%\end{verbatim}
%Die Angaben erweitern Mosquitto unter anderem mit einer persistenten Speicherung.
%Des Weiteren wird hier der Pfad für persistente Daten angegeben.

%Für weitere Konfiguration lässt sich die Datei beliebig erweitern.
Die Datei lässt sich aber auch mit weiterer Konfiguration beliebig erweitern.
So kann Mosquitto zum Beispiel noch mit einer Authentifizierung oder SSL/TLS Support ausgestattet werden.

\subsubsection{Telegraf}

In der \textit{telegraf.conf}-Datei werden allgemeine Einstellungen getroffen.
Unter Anderem wird dort das Intervall festgelegt, indem die Daten weitergeleitet werden.
%\begin{verbatim}
%    flush_interval = "10s"
%\end{verbatim}
Außerdem werden dort die Telegraf-Plugins konfiguriert.\\
Als Input-Plugin wird das \textit{mqtt\_consumer}-Plugin verwendet.
Eingestellt wird auf welchen Server gehört wird, auf welche Topics und in welchem Format Daten entgegengenommen werden.
%\begin{verbatim}
%    ## Broker URLs for the MQTT server or cluster.
%    servers = ["tcp://mosquitto:1883"]
%
%    ## Topics that will be subscribed to.
%    topics = [
%    "sensors",
%    ]
%
%    ## Data format to consume.
%    data_format = "influx"
%\end{verbatim}

Als Output-Plugin wird \textit{influxdb} verwendet.
Hier muss nur der Server und der Name der Datenbank angegeben werden.
%\begin{verbatim}
%     urls = ["http://influxDB:8086"]
%     database = "$INFLUXDB_DB"
%\end{verbatim}

\subsection{Webinterface}

Um die Daten dem Nutzer visualisiert bereitstellen zu können, kommt \textit{Grafana} zum Einsatz.
\begin{figure}[htbp]
    \centering
    \includegraphics[width=\textwidth]{img/dashboard}
    \caption{Grafana Dashboard mit Messwerten aus 20 Stunden}
\end{figure}\\
Für das automatische Verbinden mit der Influx-Datenbank wird eine \textit{influx.yaml} Datei benötigt.\cite{grafanaProvisioning}
Innerhalb der \textit{influx.yaml}-Datei gibt man alles Notwendige für die Verbindung mit unserer gewünschten Datenquellen an.\\
Wichtig sind unter anderem der Datenbanktyp der Quelle, in unserem Fall eine Influx-Datenbank, und der Name der Datenbank, auf die man zugreifen möchte.
%Ersteres da Grafana noch andere Datenbank-Anbindungen neben Influx unterstützt und entsprechend unterschieden wird.
Zur Authentifizierung muss ein Nutzer mit Zugriffsberechtigung angegeben werden.
Passwörter und Namen sind mit Umgebungsvariablen realisiert worden, damit sie nicht in mehren Dateien eingetragen werden müssen.\\
%
%Die Visualisierung entsteht mit Grafana im Browser.
%Dafür verwendet man die von Grafana zur Verfügung gestellten Werkzeuge.\\

Jeder Graph wird mit einer Datenbankabfrage für das Einholen der Messdaten versehen.
Anschließend können Typ, Skalierung, Einheit und Farbe angepasst werden.
Zuletzt speichert man das JSON-Modell des Dashboards in eine \textit{.json}-Datei.
Diese wird verwendet, um das Dashboard beim ersten Initialisieren oder nach Löschen des Volumes wieder importieren zu können.\\

\newpage

\section{Fazit}
Zusammenfassend können wir über unser Projekt sagen, dass alle Ziele ohne größere Probleme umgesetzt werden konnten.\\

Es ist absolut möglich IoT mit eigener Open-Source-Infrastruktur zu betreiben, ohne auf Komfortfunktionen verzichten zu müssen.\\
Die Wetterstation ist derzeit in Betrieb und die Messwerte erfüllen nach einem ersten Test alle von uns definierten Anforderungen.
Einzig die Batterielaufzeit könnte nach aktuellem Stand unseren Anforderungen nicht gerecht werden, ein Langzeittest steht aber noch aus.\\

Gleichwohl sind noch viele Verbesserungen möglich, einige davon sind:
\begin{itemize}
    \setlength\itemsep{0em}
    \item Ein besserer Regensensor, der die Niederschlagsmenge misst
    \item Ein VPN ins Heimnetz, um unterwegs auf die Daten zuzugreifen
    \item Ein 3D-gedrucktes Gehäuse für die Sensoreinheit
    \item Das Übermitteln und Speichern von Logs der Sensoreinheit in der Datenbank
    \item Eine eigene Platine für die Sensoreinheit entwerfen und fertigen lassen
\end{itemize}

Die größten Probleme während des Projektes waren mit der Hardware.
Da wir Informatiker und keine Elektrotechniker sind, war das aber zu erwarten.
Wir hatten zum Beispiel Probleme mit undefinierten Zuständen und kaum dokumentierten Limitierungen an bestimmten Pins des ESP8266.
Ärgerlich war auch, dass der erste BME280 ohne ersichtlichen Grund die Funktion eingestellt hat.\\

Die Entscheidung von Beginn an ein Docker-Setup für das Gateway zu verwenden war definitiv richtig.
Dadurch konnte jeder ohne Probleme das Gateway lokal starten und testen.
Auch das Setup auf dem Raspberry~Pi war, obwohl keiner von uns mit einem Pi Erfahrung hatte, dank Docker gut umsetzbar.\\

Da jeder an allen Aufgaben beteiligt war, konnten alle Erfahrung mit den verwendenden Technologien sammeln.
Besonders in anbetracht der aktuellen Umstände hat die Gruppenarbeit sehr gut funktioniert.


%sensoreinheit ~15€ ohne batterien ohne aliexpress
%Zeitplan im Gant digramm hat überraschend gut gepasst.
%Es können ohne probleme mehrere sensoreinheiten itigriert werden.
%Es muss kein PI fürs gateway sein.
%Neue technolhien waren telegraf , influx, mqtt und c++
%der timestamp der messwerte lässt sich genauer machen
%backups der db
%integration ist home assistant systeem

\newpage

\bibliographystyle{unsrt}
\bibliography{references}

\newpage

\setlength{\parindent}{0ex}
\section*{Selbständigkeitserklärung}
\pagestyle{plain}

Hiermit erklären wir die vorliegende Seminararbeit eigenständig verfasst und keine
anderen als die im Literaturverzeichnis angegebenen Quellen, Darstellungen und
Hilfsmittel benutzt zu haben. Dies trifft insbesondere auch auf Quellen aus dem Internet zu.
Alle Textstellen, die wortwörtlich oder sinngemäß anderen Werken oder sonstigen
Quellen entnommen sind, haben wir in jedem einzelnen Fall unter genauer Angabe der
jeweiligen Quelle gekennzeichnet. Uns ist bekannt, dass die Unrichtigkeit dieser Erklärung
eine Benotung der Arbeit mit der  Note "nicht ausreichend" (5.0) zur Folge hat und dass Verletzungen des Urheberrechts strafrechtlich verfolgt werden können.
\vspace{3cm}


\rule{1.0\textwidth}{0.4pt}\\
(Datum, Unterschrift, Matrikelnummer)

\vspace{3cm}
\rule{1.0\textwidth}{0.4pt}\\
(Datum, Unterschrift, Matrikelnummer)

\vspace{3cm}
\rule{1.0\textwidth}{0.4pt}\\
(Datum, Unterschrift, Matrikelnummer)

% ENDE INHALT ****************************************************************

\end{document}
