# Weatherstation

## Docker Setup

### Prerequisites
- docker
- docker-compose
- direnv (optional)

### Run container
Use `docker-compose up` to start all services required for the gateway.
 
### Environment variables
You need to setup all environment varaibles listed in *.envrc*. 
You can do this manually or by using direnv. 

## MQTT Message
Measurements must be published to the `sensor` topic.
The message must be in the *InfluxDB line protocol* format.
```
weather,sensorunit=1 temperature=21,humidity=36,preassure=1123,rainfall=1,voltage=3.17
```
To publish test messages you may install mosquitto on your device and use mosquitto_pub to publish the message.
```
mosquitto_pub -t "sensors" -m "weather,sensorunit=1 temperature=21,humidity=36,preassure=1123,rainfall=1,voltage=3.17" -h <HOSTNAME> 
```

## InfluxDB
Connect to the container via `docker exec`.
To access the database use the following command.
```
influx -precision rfc3339 -database <DB_NAME>
```
From here InfluxQL can be used to create custom queries.
E.g. get all from the measurement `weather`.
```
SELECT * FROM weather
```