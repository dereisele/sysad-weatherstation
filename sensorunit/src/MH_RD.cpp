#include <Arduino.h>

class MH_RD
{
private:
    int8_t enabledPin_;
    int8_t dataPin_;
public:
    MH_RD(int8_t enabledPin, int8_t dataPin);
    bool readSensor();
};

MH_RD::MH_RD(int8_t enabledPin, int8_t dataPin)
{
    enabledPin_ = enabledPin;
    pinMode(enabledPin_, OUTPUT);
    dataPin_ = dataPin;
    pinMode(dataPin_, INPUT);
}

bool MH_RD::readSensor(){
    bool isRaining;
    digitalWrite(enabledPin_, HIGH);
    delay(100);
    isRaining = !digitalRead(dataPin_);
    digitalWrite(enabledPin_, LOW);
    return isRaining;
}