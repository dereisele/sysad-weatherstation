#include <PubSubClient.h>
#include <WiFiClient.h>

class MQTTClient: public PubSubClient
{
public:
    MQTTClient(Client& client): PubSubClient(client){};
    bool connect(String server, uint16_t port);
    bool sendMsg(String topic, String msg);
};
